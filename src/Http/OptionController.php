<?php

namespace Frebato\Gucci\Http;

use App\DataTables\OptionDataTable;
use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Types\DateTime;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Frebato\Gucci\Services\Option as ServiceOption;

class OptionController extends Controller
{
    /**
     * @param Request $request
     */
    public function all(Request $request)
    {
        $option = new Option;
        $dataTable = new OptionDataTable;

        $warning = $request->session()->get('warning');
        $info = $request->session()->get('info');
        $nameDisplay = $option->nameDisplay;
        $routeNew = '';
        $extraButtons = [];
        $deleteAllButton = $option->dashboardActions('destroy');

        return $dataTable->render(
            'gucci.list',
            compact(
                'warning',
                'info',
                'nameDisplay',
                'routeNew',
                'extraButtons',
                'deleteAllButton'
            )
        );
    }

    /**
     * @param Request $request
     * @param null|int $id
     */
    public function view(Request $request, ?int $id)
    {
        try {
            $option = Option::buildOption($id);
        } catch (\Exception $e) {
            $request->session()->flash('warning', $e->getMessage());
        }

        foreach ($option->dto->fields as $property => $attr) {
            if (DateTime::class === $attr['type']) {
                if (null !== $option->{$property}) {
                    $option->{$property} = Carbon::createFromFormat('Y-m-d H:i:s', $option->{$property})
                        ->format('d/m/Y');
                }
            }
        }

        $html = $option->asHTML();
        $warning = $request->session()->get('warning');
        $info = $request->session()->get('info');

        return view('gucci.view', compact('html', 'warning', 'info'));
    }

    /**
     * @param Request $request
     * @param null|int $id
     *
     * @return RedirectResponse
     */
    public function store(Request $request, ?int $id): RedirectResponse
    {
        $option = Option::find($id) ?? new Option(compact('id'));
        $option->id = $id;

        $data = $request->all();

        unset(
            $data['_token'],
            $data['_method'],
            $data['redirect-to-referer'],
            $data['name']
        );

        $option = ServiceOption::store($data, $option);

        if (null === $option) {
            return redirect()->back()->withInput();
        }

        $option->save();

        $request->session()->flash(
            'info',
            'Option mise à jour',
        );

        return redirect(route('admin'));
    }
}
