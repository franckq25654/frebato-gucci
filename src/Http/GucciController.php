<?php

namespace Frebato\Gucci\Http;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Frebato\Gucci\Services\GucciService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Option;

class GucciController extends Controller
{
    public function index(Request $request): View
    {
        $warning = $request->session()->get('warning');
        $info = $request->session()->get('info');

        return view('gucci.home', compact('warning', 'info'));
    }

    public function all(Request $request)
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITHOUT_PARAM, false);
        } catch (\Exception $e) {
            $request->session()->flash('warning', 'Element introuvable');

            return redirect(route('admin'));
        }

        return GucciService::all($request, $slug);
    }

    public function view(Request $request, ?int $id)
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_ONE_PARAM, false);
        } catch (\Exception $e) {
            $request->session()->flash('warning', 'Element introuvable');

            return redirect(route('admin'));
        }

        return GucciService::view($request, $slug, $id, '%s.store');
    }

    public function store(Request $request, ?int $id): RedirectResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_TWO_PARAM, false);
        } catch (\Exception $e) {
            $request->session()->flash('warning', 'Element introuvable');

            return redirect(route('admin'));
        }

        return GucciService::store($request, $slug, $id);
    }

    public function destroy(Request $request, int $id): JsonResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_TWO_PARAM, true);
        } catch (\Exception $e) {
            throw new \RuntimeException('Element introuvable', Response::HTTP_BAD_REQUEST);
        }

        return GucciService::destroy($request, $slug, $id);
    }

    public function forceDestroy(Request $request, int $id): JsonResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_ONE_PARAM, true);
        } catch (\Exception $e) {
            throw new \RuntimeException('Element introuvable', Response::HTTP_BAD_REQUEST);
        }

        return GucciService::forceDestroy($request, $slug, $id);
    }

    public function restore(Request $request, int $id): JsonResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_ONE_PARAM, true);
        } catch (\Exception $e) {
            throw new \RuntimeException('Element introuvable', Response::HTTP_BAD_REQUEST);
        }

        return GucciService::restore($request, $slug, $id);
    }

    public function deleteAttachment(Request $request, int $id, string $name): JsonResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITH_TWO_PARAM, true);
        } catch (\Exception $e) {
            throw new \RuntimeException('Element introuvable', Response::HTTP_BAD_REQUEST);
        }

        return GucciService::deleteAttachment($request, $slug, $id, $name);
    }

    public function addResource(Request $request): JsonResponse
    {
        try {
            $slug = Helper::resolveURL(Helper::URL_WITHOUT_PARAM, true);
        } catch (\Exception $e) {
            throw new \RuntimeException('Element introuvable', Response::HTTP_BAD_REQUEST);
        }

        return GucciService::addResource($request, $slug);
    }

    public function leave(Request $request): RedirectResponse
    {
        $manager = app('impersonate');

        if (!$manager->isImpersonating()) {
            abort(403);
        }

        $manager->leave();

        return redirect()->back();
    }
}
