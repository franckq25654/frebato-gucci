/*!
 * jQuery Rebind v1.0
 *
 * Copyright 2018
 */
 
(function(jQuery){
	jQuery.extend(jQuery.fn,
	{		
		_on: function(events, handler, data){
			events	= (typeof events === 'string') ? events : '';
			handler	= (typeof handler === 'function') ? handler : function(){};
			data	= (typeof data === 'object') ? data : {_this : null};
			
			this.on(events, data, handler);
			
			return this;
		},
		rebind: function(events, handler, data){
			events	= (typeof events === 'string') ? events : '';
			handler	= (typeof handler === 'function') ? handler : function(){};
			data	= (typeof data === 'object') ? data : {_this : null};
			
			this.off(events, handler).on(events, data, handler);
			
			return this;
		}
	});
})(jQuery);
