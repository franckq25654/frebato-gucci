class Gucci {
    constructor() {}
    init() {
        this.rebindAppEvents();
        this.initAppLayouts();
    }
    initDataTables() {
        $('#delete-selected').html('Supprimer').prop("disabled", true)
        this.init()
    }
    initWysiwyg() {
        $('.wysiwyg').each((index, item) => {
            ClassicEditor
                .create(document.getElementById(item.id))
                .catch(error => {
                        console.log(`There was a problem initializing the editor.`, error)
                    }
                );
        });
    }
    rebindAppEvents() {
        $('.to-confirm')
            .off('click', this.toConfirm)
            .on('click', this.toConfirm)
        $('.scroll-to')
            .off('click', this.clickScrollToHandler)
            .on('click', this.clickScrollToHandler)
        $('ul.nav.nav-sidebar .nav-item')
            .off('click', this.overrideClickMenuHandler)
            .on('click', this.overrideClickMenuHandler)
        $('.selected-rows')
            .off('change', this.changeSelectedRowsHandler)
            .on('change', this.changeSelectedRowsHandler)
        $('#delete-selected')
            .off('click', this.deleteSelectedHandler)
            .on('click', this.deleteSelectedHandler)
        $('.to-disable')
            .off('click', this.preventMultipleSubmit)
            .on('click', this.preventMultipleSubmit)
    }
    initAppLayouts() {
        $('input.datetimepicker').each((index, _this) => {
            let defaultDate = $(_this).val()

            if ('' !== defaultDate && defaultDate.includes('/')) {
                defaultDate = moment(defaultDate, 'DD/MM/YYYY')
            } else if ('' !== defaultDate) {
                defaultDate = moment(defaultDate)
            } else if ('' === defaultDate) {
                defaultDate = moment()
            }

            let opt = {
                locale: 'fr',
                format: 'L',
                defaultDate: defaultDate.format('YYYY-MM-DD'),
                inline: true,
                sideBySide: true,
                keepOpen: true,
                collapse: false,
                debug: true,
                icons: {
                    time: 'far fa-calendar-times',
                    date: 'far fa-calendar-times',
                    up: 'fas fa-caret-up',
                    down: 'fas fa-caret-down',
                    previous: 'fas fa-caret-left',
                    next: 'fas fa-caret-right',
                    today: 'fas fa-calendar-day'
                }
            }

            $(_this)
                .datetimepicker(opt)

            if ($(_this).hasClass('disabled')) {
                $(_this)
                    .on('dp.change', this.preventChangeDP)
            }

            $(_this).hide()
        })

        // $('#slick').slick()

        $('.select-2').each(function() {
            $(this).select2()
        })

        $('input[data-mask]').each(function() {
            let data = $(this).data('mask')

            if (typeof data !== "undefined") {
                $(this).mask(data.mask, {reverse: true})
            }
        })

        $('.slick').each(function() {
            let slidesToShow = parseInt($(this).data('slides-to-show')) > 0
                ? $(this).data('slides-to-show')
                : 1;
            let slidesToScroll = parseInt($(this).data('slides-to-scroll')) > 0
                ? $(this).data('slides-to-scroll')
                : 1;
            let autoplaySpeed = typeof $(this).data('autoplay-speed') !== 'undefined'
                ? $(this).data('autoplay-speed')
                : 2000;
            $(this).slick({
                slidesToShow: slidesToShow,
                slidesToScroll: slidesToScroll,
                autoplay: true,
                autoplaySpeed: autoplaySpeed
            });
        })
    }
    preventChangeDP(e) {
        $(e.target).off('dp.change', (new Functions).preventChangeDP)
        $(e.target).data("DateTimePicker").date(e.oldDate)
        $(e.target).on('dp.change', (new Functions).preventChangeDP)
    }
    toConfirm(e) {
        e.preventDefault()
        let item = $(this);
        let form = $(this).parent('form')
        let url = $(this).attr('href')
        let remove_mode = $(this).hasClass('remove')

        // @Todo spécial ISULA
        let plural = $(this).hasClass('remove-booking') && parseInt($(this).data('count')) > 1
            ? "s"
            : ""
        let text = $(this).hasClass('remove-booking')
            ? "La réservation que vous souhaitez supprimer entrainera la suppression " +
            "de " + $(this).data('count') + " entrée" + plural + " dans le planning, continuer ?"
            : "cette action est irreversible, continuer ?"

        let callback = typeof $(this).data('callback') !== 'undefined'
            ? () => {eval($(this).data('callback'))}
            : null;
        url = (typeof url === 'undefined')
            ? form.attr('action')
            : url
        let _method = (form.find('input[name="_method"]').length > 0) ?
            form.find('input[name="_method"]').val() :
            ('' === $(this).data('method')
                ? 'GET'
                : $(this).data('method'))

        Swal.fire({
            title: 'Attention,',
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: url,
                    method: _method,
                    dataType: 'json',
                    context: this,
                    success: function(data) {
                        if (data.status === 'success') {
                            iziToast.show({
                                message: data.message,
                                color: 'green'
                            })

                            if (remove_mode) {
                                item.parents('tr').fadeOut(400, function() {
                                    $(this).remove();
                                })

                                item.parents('.form-group.mb-3').fadeOut(400, function() {
                                    $(this).remove();
                                })

                                item.parents('.file-preview-frame.kv-preview-thumb').fadeOut(400, function() {
                                    $(this).remove();
                                })
                            }

                            if (null !== callback) {
                                callback();
                            }
                        } else {
                            iziToast.show({
                                message: 'Un problème est survenue lors de la suppression de cet élement',
                                color: 'red'
                            })
                        }
                    }
                })
            }
        })
    }
    clickScrollToHandler(e) {
        e.preventDefault()

        let elementToScrollToID = $(e.currentTarget).attr('href')

        $([document.documentElement, document.body]).animate({
            scrollTop: $(elementToScrollToID).offset().top
        }, 500)
    }
    overrideClickMenuHandler(e) {
        let link = $(e.currentTarget).find('a').attr('href')

        if ('#' !== link) {
            e.preventDefault()
            e.stopPropagation()
            location.href = $(e.currentTarget).find('a.nav-link').attr('href')
        }
    }
    changeSelectedRowsHandler(e) {
        console.log('changeSelectedRowsHandler')
        let nbSelected = $('.selected-rows:checked').length

        if (nbSelected <= 0) {
            $('#delete-selected').html('Supprimer').prop("disabled", true);
        } else {
            $('#delete-selected').html('Supprimer (' + nbSelected + ')').prop("disabled", false);
        }
    }
    deleteSelectedHandler(e) {
        Swal.fire({
            title: 'Attention,',
            text: 'Vous vous appretez à supprimer plusieurs élements, êtes-vous sûr ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $('.selected-rows:checked').each(function (i) {
                    let _this = $('.selected-rows:checked').eq(i)

                    $.ajax({
                        url: '/admin/' + _this.data('slug') + '/destroy/' + _this.val(),
                        method: 'delete',
                        dataType: 'json',
                        context: this,
                        success: function () {
                            $('table.dataTable')
                                .find('.selected-rows[value="' + _this.val() + '"]')
                                .parents('tr')
                                .fadeOut(400, function() {
                                    $(this).remove();
                                })
                        }
                    })
                })

                $('#delete-selected').html('Supprimer').prop("disabled", true);
            }
        })
    }
    preventMultipleSubmit(e) {
        e.preventDefault();
        $(e.currentTarget).prop("disabled", true);
        jQuery(e.currentTarget).parents('form').submit();

        setTimeout(function() {
            $(e.currentTarget).prop("disabled", false);
        }, 5000);
    }
}

let _gucci = new Gucci();

$(document).ready(() => {
    _gucci.init();
    _gucci.initWysiwyg();
});

$(document).on('dt.draw', () => {
    _gucci.initDataTables();
});
