<?php

namespace App\Traits;

use App\DTO\DTO;

trait HasDTO
{
	public function getDtoAttribute(): DTO
	{
        $class = sprintf('\\App\\DTO\\%s', class_basename(get_class()));

        if (! class_exists($class)) {
            throw new \Exception(sprintf('%s doesn\'t exists, ae you forget to run `php artisan gucci:make-dto` ?', class_basename($class)));
        }

        return new $class($this);
	}
}
