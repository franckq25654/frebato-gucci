<?php

namespace App\Types;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

abstract class Model
{
    public static function migrationMethod()
    {
        return null;
    }

    public static function templateForm(
        $attr,
        $property = '',
        $value = '',
        $hidden = false,
        $update = false,
        $model = null
    ) {
        [$relationType, $related, $method] = Helper::getRelationType($model, $property);
        $related = new $related;
        $multiple = (BelongsToMany::class === $relationType || HasMany::class === $relationType)
            ? ' multiple="multiple"'
            : '';
        $disabled = !empty($attr['disable'])
            ? ' disabled'
            : '';
        $require = !empty($attr['require'])
            ? ' required'
            : '';
        $requireTxt = !empty($attr['require'])
            ? ' <span class="require">*</span>'
            : '';
        $isBrackets = (empty($multiple))
            ? ''
            : '[]';
        $hidden = $hidden
            ? ' hide'
            : '';
        $name = (!isset($attr['disable']) || FALSE === $attr['disable'])
            ? sprintf(' name="%s%s"', $property, $isBrackets)
            : '';
        $label = $attr['label'] ?? $property;

        try {
            $a = $value?->id;
        } catch (\Exception $e) {
            // dd($relationType);
        }

        $myValues = (BelongsToMany::class === $relationType || HasMany::class === $relationType)
            ? ($value ?? [])
            : (is_object($value)
                ? ($value?->id
                    ? [$value->id]
                    : [])
                : ($value
                    ? [$value]
                    : []));

        if (empty($attr['where'])) {
            $values = $related::all();
        } else {
            $ids = DB::select(sprintf('SELECT id FROM %s WHERE %s', $related->getTable(), $attr['where']));

            foreach ($ids as $k => $id) {
                $ids[$k] = $id->id;
            }

            $values = $related->whereIn('id', $ids)->get();
        }

        if (null !== $model->dto->adminEditFilter($related, $attr)) {
            $ids = $model->dto->adminEditFilter($related, $attr);
            $values = $related->whereIn('id', $ids)->get();
        }

        $html = sprintf('<div class="form-group my-3%s">', $hidden);
        $html .= sprintf('<label for="%s">%s%s</label>', $property, $label, $requireTxt);
        $html .= sprintf('<select id="%s"%s%s class="form-control select-2"%s%s>', $property, $name, $multiple, $require, $disabled);
        $html .= '<option value="" data-index="0"></option>';

        if (null !== $values) {
            $values->each(function($current, $i) use ($update, $myValues, &$html) {
                $index = $i + 1;
                if (is_object($current)) {
                    $isSelected = '';

                    if ($update) {
                        $isSelected = (in_array($current->id, $myValues))
                            ? ' selected'
                            : '';
                    }

                    $currentValue = Helper::usualDisplay($current) ?? '';

                    $html .= sprintf('<option value="%s" data-index="%s"%s>%s</option>', $current->id, $index, $isSelected, $currentValue);
                }
            });
        }

        $html .= '</select>';
        $html .= '</div>';
        $html .= '<script type="application/javascript">';
        $html .= 'window.onload = function() {';
        $opt = !empty($attr['tags'])
            ? '{tags: true}'
            : '';
        $binder = method_exists($model,'binder')
            ? $model->binder($property, $opt)
            : '';
        $html .= sprintf('$("#%s").select2(%s)%s;', $property, $opt, $binder);
        $html .= '};';
        $html .= '</script>';

        return $html;
    }
}
