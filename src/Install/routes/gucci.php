<?php

use Frebato\Gucci\Http\GucciController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Auth::routes(['register' => false]);
Route::impersonate();

Route
    ::get(
        '/',
        [GucciController::class, 'index']
    )
    ->name('admin')
    ->middleware(['web', 'auth'])
;
