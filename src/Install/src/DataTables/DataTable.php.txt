<?php

namespace App\DataTables;

use App\Helpers\Helper;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable as OriginalDataTable;
use App\Traits\DataTableTrait;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class BaseModelDataTable extends OriginalDataTable
{
    use DataTableTrait;

    private string $routeNamePrefix;

    public function __construct(string $routeNamePrefix = '')
    {
        $this->routeNamePrefix = $routeNamePrefix;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = datatables()
            ->eloquent($query)
            ->addColumn('action', function ($item) {
                $buttons = '';
                $_buttons = $this->buttons($item);

                foreach ($_buttons as $button) {
                    $buttons .= sprintf('<p>%s</p>', $button);
                }

                if (true === $item->dto->dashboardActions('destroy')) {
                    $buttons .= sprintf(
                        '<input name="selected-rows[]" class="selected-rows" type="checkbox" value="%s" data-slug="%s" />',
                        $item->id,
                        $item->getTable()
                    );
                }

                return $buttons;
            })
        ;

        $dataTable = $dataTable->removeColumn($this->hiddenProperties());

        // Dynamic dates can be specify here
        #DYNAMIC_DATES#

        // Edit Columns can be specify here
        #EDIT_COLUMNS#

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\BaseModel $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BaseModel $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('model-table')
            ->addTableClass('data-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->lengthMenu()
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [];
        $hiddenProperties = $this->hiddenProperties();

        $columns[] = Column::computed('action')
            ->title(__('Action'))
            ->exportable(true)
            ->printable(true)
            ->addClass('align-middle text-center mx-5')
            ->width('120px');

        #GET_COLUMNS#

        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Export_' . date('YmdHis');
    }

    private function hiddenProperties()
    {
        $hiddenProperties = [];
        $user = Auth::user();

        return $hiddenProperties;
    }

    private function buttons(BaseModel $model)
    {
        $buttons = [];
        $user = Auth::user();
        $routeNamePrefix = empty($this->routeNamePrefix)
            ? ''
            : sprintf('%s.', $this->routeNamePrefix);

        $buttons['see_route'] = Helper::button(
            route($routeNamePrefix . 'className.view', ['id' => $model->id]),
            '<i class="far fa-eye"></i> Voir',
            'get',
            'px-3 btn btn-xs btn-primary',
            [
                'title' => 'Voir',
                'style' => 'font-size:14px',
            ]
        );

        if (true === $model->dto->dashboardActions('destroy')) {
            $buttons['del_route'] = Helper::button(
                route($routeNamePrefix . 'className.destroy', ['id' => $model->id]),
                '<i class="far fa-trash-alt"></i> Supprimer',
                'get',
                'remove to-confirm px-3 btn btn-xs btn-danger',
                [
                    'title' => 'Supprimer',
                    'style' => 'font-size:12px',
                    'data-method' => 'delete',
                ]
            );
        }

        return $buttons;
    }
}
