<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Models\Logger;
use App\Models\Option;
use App\Models\OptionConf;
use App\Models\OptionField;
use App\Models\Slot;
use Illuminate\Database\Seeder;
use App\Models\Level;
use App\Models\User;
use App\Models\Apartment;
use App\Models\City;
use App\Models\Hour;
use App\Models\AppOpt;
use App\Models\Region;
use App\Models\Residence;
use App\Models\Typology;
use Illuminate\Support\Carbon;
use Frebato\Gucci\Services\Option as ServiceOption;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->runLevelSeeder();
        $this->runUserSeeder();
    }

    private function runLevelSeeder()
    {
        $levels = $this->sourceFix('levels');

        foreach ($levels as $level) {
            $seed = Level::where('name', '=', $level['name'])?->first();

            if (null === $seed ) {
                $seed = new Level;
                $seed->setAttribute('id', $level['ID']);
                $seed->setAttribute('name', $level['name']);
                $seed->setAttribute('value', $level['value']);
                $seed->save();
            }
        }
    }

    private function runUserSeeder()
    {
        $level_user = $this->sourceFix('level_user');
        $users = $this->sourceFix('users');

        foreach ($users as $user) {
            foreach ($level_user as $data) {
                if ($data['user_id'] == $user['ID']) {
                    $level = Level::find($data['level_id']);
                    break;
                }
            }

            $seed = User::where('email', '=', $user['email'])?->first();

            if (null === $seed ) {
                $seed = new User();
                $seed->setAttribute('id', $user['ID']);
                $seed->setAttribute('name', $user['name']);
                $seed->setAttribute('email', $user['email']);
                // $seed->setAttribute('logo', str_replace('/home/isulape/www/public/uploads/', 'storage/', $user['logo']));

                if ('franckq@hotmail.com' === $user['email']) {
                    $seed->setAttribute('password', Hash::make($user['email']));
                } else {
                    $seed->setAttribute('password', Hash::make($user['email'] . '&99!'));
                }

                $seed->setAttribute('level_id', $level->id);
                $seed->save();
            }
        }
    }

    private function sourceFix(string $name): array
    {
        $fileContent = file_get_contents(__DIR__ . sprintf('/sources/%s.php', $name));
        $fileContent = str_replace(sprintf('$%s = ', $name), 'return ', $fileContent);
        file_put_contents(__DIR__ . sprintf('/sources/%s.php', $name), $fileContent);

        return  require(__DIR__ . sprintf('/sources/%s.php', $name));
    }
}
