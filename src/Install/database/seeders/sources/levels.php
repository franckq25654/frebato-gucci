<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version OVH
 */

/**
 * Database `isulapemain`
 */

/* `isulapemain`.`levels` */
return array(
  array('ID' => '1','name' => 'superadmin','value' => '10000','created_at' => NULL,'updated_at' => NULL),
  array('ID' => '2','name' => 'Admin','value' => '1000','created_at' => NULL,'updated_at' => NULL),
  array('ID' => '3','name' => 'User','value' => '100','created_at' => NULL,'updated_at' => NULL),
);
