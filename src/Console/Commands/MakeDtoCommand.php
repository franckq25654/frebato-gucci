<?php

namespace Frebato\Gucci\Console\Commands;

use Frebato\Gucci\Services\MakeDtoService;
use Illuminate\Console\Command;

class MakeDtoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gucci:make-dto
                            {model? : if not specified, all models}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run `gucci:install` before';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            MakeDtoService::execute($this->argument('model'));
        } catch (\Exception $e) {
            $this->warn($e->getMessage());

            return 1;
        }

        $this->info('DTOs files has been created.');
        $this->comment('OK!');

        return 0;
    }
}
