<?php

namespace Frebato\Gucci\Console\Commands;

use Frebato\Gucci\Services\GenerateMigrationService;
use Illuminate\Console\Command;

class GenerateMigrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gucci:generate-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run `gucci:make-dto` before';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GenerateMigrationService::execute();
        $this->info('Migration files has been created.');
        $this->comment('OK!');

        return 0;
    }
}
