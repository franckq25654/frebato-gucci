<?php

namespace Frebato\Gucci\Console\Commands;

use Frebato\Gucci\Services\MakeDataTablesService;
use Illuminate\Console\Command;

class MakeDataTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gucci:make-datatables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run `gucci:make-dto` before';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        MakeDataTablesService::execute();
        $this->info('DataTables files has been created.');
        $this->comment('OK!');

        return 0;
    }
}
