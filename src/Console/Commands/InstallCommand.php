<?php

namespace Frebato\Gucci\Console\Commands;

use Frebato\Gucci\Services\InstallService;
use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gucci:install
                            {--force : overwrite existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run it first';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        InstallService::execute($this->option('force'));
        $this->info('Installation has been finished correctly.');
        $this->comment('OK!');

        return 0;
    }
}
