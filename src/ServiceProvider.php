<?php namespace Frebato\Gucci;

use App\Helpers\Helper;
use App\Models\Level;
use App\Models\OptionConf;
use App\Models\OptionField;
use App\Observers\ModelObserver;
use App\Observers\OptionConfObserver;
use App\Observers\OptionFieldObserver;
use Frebato\Gucci\Console\Commands\InstallCommand;
use Frebato\Gucci\Console\Commands\GenerateMigrationCommand;
use Frebato\Gucci\Console\Commands\MakeDtoCommand;
use Frebato\Gucci\Console\Commands\MakeDataTablesCommand;
use Frebato\Gucci\Services\Dashboard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        // Get namespace
        $nameSpace = $this->app->getNamespace();

        // Routes
        $this->app->router->group(['namespace' => $nameSpace . 'Http\Controllers'], function() {
            require __DIR__ . '/routes.php';
        });

        // Register the command if we are using the application via the CLI
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                GenerateMigrationCommand::class,
                MakeDtoCommand::class,
                MakeDataTablesCommand::class,
            ]);
        }

        // Views
        $this->publishes([
            __DIR__ . '/../views' => base_path('resources/views'),
            __DIR__ . '/../views/components' => base_path('resources/views/components'),
            __DIR__ . '/../views/components/gucci' => base_path('resources/views/components/gucci'),
            __DIR__ . '/../views/gucci' => base_path('resources/views/gucci'),
        ], 'frebato-gucci');

        View::composer('*', function ($view) {
            if (Auth::check()) {
                $view->with('user', Auth::user());
            }
        });

        View::composer('gucci.base', function ($view) {
            $title = config(sprintf('titles.%s', Route::currentRouteName()), config('app.name', ''));
            $menu = Dashboard::getMenu();
            $view->with(compact('title', 'menu'));
        });

        Blade::if('isAdmin', function () {
            return Auth::user()?->role === Level::ADMIN_VALUE;
        });

        Blade::if('superadmin', function () {
            return Auth::user()?->role === Level::SUPERADMIN_VALUE;
        });

        Blade::if('role', function ($roles) {
            $roles = is_array($roles)
                ? $roles
                : [$roles];
            $roles = Level::whereIn('name', $roles)->get()->pluck('value')->toArray();

            return in_array(Auth::user()?->role, $roles);
        });

        if (class_exists(Helper::class)) {
            foreach (Helper::getModels() as $model) {
                resolve($model)->observe(ModelObserver::class);
            }
        }

        if (class_exists(OptionField::class) && class_exists(OptionFieldObserver::class)) {
            OptionField::observe(OptionFieldObserver::class);
        }

        if (class_exists(OptionConf::class) && class_exists(OptionConfObserver::class)) {
            OptionConf::observe(OptionConfObserver::class);
        }
    }

    public function register() {}
}
