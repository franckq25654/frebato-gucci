<?php

namespace Frebato\Gucci\Services;

use App\Helpers\Helper;
use App\Models\User;
use App\Traits\HasDTO;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;
use App\Types\Model as TypeModel;

class GenerateMigrationService
{
    public static function execute()
    {
        // Migrations
        foreach (Helper::getModels() as $model) {
            if (! trait_exists(HasDTO::class) || ! in_array(HasDTO::class, class_uses_recursive($model))) {
                continue;
            }

            $item = new $model;
            // Now, we can make the process
            $filename = sprintf('%s_handle_%s_table', (new \DateTime())->format('Y-m-d His'), $item->getTable());
            $localPath = base_path(sprintf('/database/migrations/%s.php', $filename));
            $fileContent = file_get_contents(__DIR__ . '/../Install/src/migrations/HandleModelTable.php.txt');

            $fileContent = str_replace([
                '#CLASS_TABLE#',
                '// HandleModelTable::up();',
                '// HandleModelTable::down();',
            ], [
                $item->getTable(),
                self::generateFunctionUp($item),
                self::generateFunctionDown($item),
            ], $fileContent);

            file_put_contents($localPath, $fileContent);
        }
    }

    /**
     * @param Model $model
     * @return string
     */
    private static function generateFunctionUp(Model $model): string
    {
        $php = '';
        $extraPivotPhp = [];

        $action = is_a($model, User::class, true)
            ? 'table'
            : 'create';

        $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp2.txt');
        $php .= str_replace(['#ACTION#', '#TABLE#'], [$action, $model->getTable()], $_php);

        if (!is_a($model, User::class, true)) {
            $php .= file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp3.txt');
        }

        foreach ($model->dto->fields as $name => $attributes) {
            if (is_a($model, User::class, true) && in_array($name, [
                    'name',
                    'email',
                    'password',
                    'email_verified_at',
                ])) {
                continue;
            }

            if ('deleted_at' === $name) {
                continue;
            }

            $type = $attributes['type'];
            $options = isset($attributes['options']) && is_array($attributes['options'])
                ? sprintf(", ['%s']", implode("', '", $attributes['options']))
                : '';

            // Si propriété `normale`
            if (TypeModel::class !== $type) {
                $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp4.txt');
                $php .= str_replace(
                    [
                        '#METHOD#',
                        '#NAME#',
                        '#OPTIONS#'
                    ],
                    [
                        $type::migrationMethod(),
                        $name,
                        $options
                    ],
                    $_php
                );

                if (!isset($attributes['require']) || (isset($attributes['require']) && false === $attributes['require'])) {
                    $php .= "\n\t\t->nullable()";
                }
                // Si relation
            } else {
                [$relationType, $related, $method] = Helper::getRelationType($model, $name);

                // Si n:n
                if (BelongsToMany::class === $relationType) {
                    $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp5.txt');
                    $extraPivotPhp[] .= str_replace(
                        [
                            '#FIRST_TABLE#',
                            '#LAST_TABLE#',
                            '#PIVOT_TABLE#',
                            '#FIRST_KEY#',
                            '#LAST_KEY#',
                        ],
                        Helper::getPivotIds($model, (new $related), $method),
                        $_php
                    );
                    // si 1:1 ou 1:n
                } elseif (BelongsTo::class === $relationType) {
                    $classTable = Helper::getValue($model->{$method}(), 'related')->getTable();
                    $classKey = Helper::getValue($model->{$method}(), 'foreignKey');
                    $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp6.txt');
                    $php .= str_replace(
                        [
                            '#CLASS_KEY#',
                            '#CLASS_TABLE#',
                        ],
                        [
                            $classKey,
                            $classTable,
                        ],
                        $_php
                    );
                }
            }

            if (!empty($attributes['unique'])) {
                $php .= "\n\t->unique()";
            }

            if (!empty($attributes['default']) && !preg_match('{^sql\:}', $attributes['default'])) {
                $php .= "\n\t" . sprintf('->default("%s")', $attributes['default']);
            }

            $php .= ';' . "\n";
        }

        if (!is_a($model, User::class, true)) {
            $php .= file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp7.txt');
        } else {
            $php .= file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionUp8.txt');
        }

        return $php . "\n\n" . implode("\n\n", $extraPivotPhp);
    }

    /**
     * @param Model $model
     * @return string
     */
    private static function generateFunctionDown(Model $model): string
    {
        $php = '';

        foreach ($model->dto->fields as $name => $attributes) {
            $type = $attributes['type'];

            if (TypeModel::class === $type) {
                $name = method_exists($model, $name)
                    ? $name
                    : preg_replace('{\_id$}', '', $name);

                if (!method_exists($model, $name)) {
                    continue;
                }

                $relationType = get_class($model->{$name}());

                // Si n:n
                if (BelongsToMany::class === $relationType) {
                    $related = get_class($model->{$name}()->getRelated());
                    $relatedKey = (new $related)->getForeignKey();

                    $key1 = str_replace('_id', '', $model->getForeignKey());
                    $key2 = str_replace('_id', '', $relatedKey);
                    $firstKey = $key1 < $key2
                        ? $key1
                        : $key2;
                    $lastKey = $key1 < $key2
                        ? $key2
                        : $key1;
                    $pivotTable = sprintf('%s_%s', $firstKey, $lastKey);

                    $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionDown1.txt');
                    $php .= str_replace(
                        [
                            '#PIVOT_TABLE#',
                            '#FOREIGN_KEY#',
                            '#CLASS_KEY#',
                            '#CLASS_TABLE#',
                        ],
                        [
                            $pivotTable,
                            $relatedKey,
                            $model->getForeignKey(),
                            $model->getTable(),
                        ],
                        $_php
                    );
                    // si 1:1 ou 1:n
                } elseif (BelongsTo::class === $relationType) {
                    $classTable = Helper::getValue($model->{$name}(), 'related')->getTable();
                    $relatedKey = Helper::getValue($model->{$name}(), 'foreignKey');

                    $_php = file_get_contents(__DIR__ . '/../Install/src/migrations/GenerateFunctionDown2.txt');
                    $php .= str_replace(
                        [
                            '#CLASS_KEY#',
                            '#CLASS_TABLE#',
                        ],
                        [
                            $relatedKey,
                            $classTable,
                        ],
                        $_php
                    );
                }
            }
        }

        return $php;
    }
}
