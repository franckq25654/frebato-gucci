<?php

namespace Frebato\Gucci\Services;

use App\Models\Level;
use App\Models\OptionConf;

/**
 * Class Dashboard
 *
 * @package Middleware
 *
 */
class Dashboard {
    public static function getMenu(): array
    {
        $items = [];

        foreach (config('menu') as $item => $conf) {
            if (null !== static::entry($item)) {
                $items = array_merge($items, static::entry($item));
            }
        }

        return $items;
    }

    private static function entry(string $entry): ?array
    {
        $conf = config(sprintf('menu.%s', $entry));

        if (!empty($conf['hide'])) {
            return null;
        }

        if (preg_match('{^(.*)\.(.*)$}', $entry, $matches)) {
            $entry = $matches[2];
        }

        $nameDisplay = $entry;

        if (true === $conf['model']) {
            $model = sprintf('App\\Models\\%s' ,$entry);
            $model = new $model;

            if (null !== $model->dto->nameDisplay) {
                $nameDisplay = $model->dto->nameDisplay;
            }

            $conf['route'] = route(sprintf('%s.all', $model->getTable()));
        }

        $children = [];

        if (!empty($conf['children'])) {
            // static callback case
            if (2 === count($conf['children']) && is_string($conf['children'][0])) {
                $caller = $conf['children'][0];
                $callback = $conf['children'][1];
                $child = $caller::$callback();
                $children = array_merge($children, $child);
            } else {
                foreach ($conf['children'] as $subItem => $child) {
                    $key = sprintf('%s.children.%s', $entry, $subItem);
                    $children = array_merge($children, static::entry($key));
                }
            }
        }

        $item[$nameDisplay] = [
            'icon' => $conf['icon'] ?? 'file-alt',
            'min-level'   => $conf['min-level'] ?? Level::ADMIN_VALUE,
            'route' => $conf['route'] ?? '#',
        ];

        if (count($children) > 0) {
            $item[$nameDisplay]['children'] = $children;
        }

        return $item;
    }

    public static function options(): array
    {
        $options = OptionConf::all();
        $children = [];

        $options->each(function ($option) use (&$children) {
            // @Todo spécial ISULA
            if (true === config('app.haute-saison') && in_array($option->name, [
                    'Homepage - basse saison',
                    'Carnet de voyage - basse saison',
                    'Condition d\'annulation',
                    'Dates Printemps',
                    'Dates Automne',
                ])) {
                return true;
            }

            // @Todo spécial ISULA
            if (true === config('app.basse-saison') && in_array($option->name, [
                'Homepage',
                'Carnet de voyage',
            ])) {
            return true;
        }

            $children[$option->name] = [
                'icon' => 'file-alt',
                'min-level'   => Level::ADMIN_VALUE,
                'route' => route('options.view', $option->id),
            ];
        });

        // @Todo spécial ISULA
        if (config('app.haute-saison')) {
            $children['Importer un fichier CSV'] = [
                'icon' => 'file-alt',
                'min-level'   => Level::ADMIN_VALUE,
                'route' => route('import-csv'),
            ];
        }

        return $children;
    }
}
