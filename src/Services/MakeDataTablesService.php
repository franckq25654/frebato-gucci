<?php

namespace Frebato\Gucci\Services;

use App\Traits\HasDTO;
use Illuminate\Database\Eloquent\Model;
use App\Types\Boolean;
use App\Types\DateTime;
use App\Types\Password;
use App\Types\Uploadable;
use App\Types\Model as TypeModel;
use Illuminate\Support\Facades\File;

class MakeDataTablesService
{
    public static function execute()
    {
        // DataTable
        $models = File::allFiles(sprintf('%s/Models', app_path()));

        foreach ($models as $model) {
            $model = preg_replace('{\.php(~)?$}', '', $model->getBasename());
            $dataTablePath = sprintf('%s/DataTables/%sDataTable.php', app_path(), $model);

            if (file_exists($dataTablePath)) {
                continue;
            }

            $baseModel = sprintf('App\\Models\\%s', $model);
            $fileContent = file_get_contents(sprintf('%s/../Install/src/DataTables/DataTable.php.txt', __DIR__));
            $fileContent = str_replace(
                ['BaseModel', 'className', '#GET_COLUMNS#', '#DYNAMIC_DATES#', '#EDIT_COLUMNS#'],
                [$model, (new $baseModel)->getTable(), self::getColumns(new $baseModel), self::dynamicsDates(new $baseModel), self::editColumns(new $baseModel)],
                $fileContent
            );
            file_put_contents($dataTablePath, $fileContent);
        }
    }

    private static function getColumns(Model $model): string
    {
        if (! trait_exists(HasDTO::class) || ! in_array(HasDTO::class, class_uses_recursive($model))) {
            return '';
        }

        $html = [];

        foreach ($model->dto->fields as $property => $attr) {
            if (in_array($attr['type'], [
                TypeModel::class,
                Password::class,
            ]) || \is_subclass_of($attr['type'], Uploadable::class)) {
                continue;
            }

            $html[] = sprintf('if (! in_array(\'%s\', $hiddenProperties)) {', $property);
            $html[] = sprintf("\t\t\t" . '$columns[] = Column::make(\'%s\')', $property);
            $html[] = sprintf("\t\t\t\t" . '->title(\'%s\');', isset($attr['label'])
                ? \addslashes($attr['label'])
                : $property);
            $html[] = "\t\t" . '}';
        }

        return implode("\n", $html);
    }

    private static function dynamicsDates(Model $model): string
    {
        if (! trait_exists(HasDTO::class) || ! in_array(HasDTO::class, class_uses_recursive($model))) {
            return '';
        }

        $html = [];

        foreach ($model->dto->fields as $key => $attr) {
            if (DateTime::class !== $attr['type']) {
                continue;
            }

            $html[] = sprintf('$dataTable = $dataTable->editColumn(\'%s\', function ($item) {', $key);
            $html[] = sprintf('return $item->%s?->format(\'d/m/Y H:i:s\');', $key);
            $html[] = '});';
        }

        return implode("\n", $html);
    }

    private static function editColumns(Model $model): string
    {
        if (! trait_exists(HasDTO::class) || ! in_array(HasDTO::class, class_uses_recursive($model))) {
            return '';
        }

        $html = [];

        foreach ($model->dto->fields as $key => $attr) {
            if (Boolean::class === $attr['type']) {
                $html[] = sprintf('$dataTable = $dataTable->editColumn(\'%s\', function ($item) {', $key);
                $html[] = sprintf('return \'1\' == $item->%s
                ? \'oui\'
                : \'non\';', $key);
                $html[] = '});';
            }
        }

        return implode("\n", $html);
    }
}
