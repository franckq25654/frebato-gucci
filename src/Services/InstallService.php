<?php

namespace Frebato\Gucci\Services;

class InstallService
{
    public static function execute(bool $force)
    {
        static::__init();
        static::copyFiles($force);
    }

    private static function __init()
    {
        $paths = [
            'css',
            'css/bootstrap',
            'css/slick',
            'css/fontawesome',
            'css/fontawesome/css',
            'css/fontawesome/webfonts',
            'js',
            'js/libs',
            'js/libs/bootstrap',
            'js/libs/ckeditor',
            'js/libs/ckeditor/translations',
            'js/libs/jquery',
            'js/libs/moment',
        ];

        foreach ($paths as $path) {
            $path = public_path($path);

            if (! is_dir($path)) {
                mkdir($path);
            }
        }

        $path = [];

        foreach (static::getInstallFiles(__DIR__ . '/../Install/') as $file) {
            $curPath = dirname($file);
            $curPath = preg_replace('{^' . preg_quote(__DIR__ . '/../Install/') . '}', '', $curPath);
            $curPath = base_path($curPath);

            if (in_array($curPath, $path)) {
                continue;
            }

            if (! is_dir($curPath)) {
                mkdir($curPath);
            }

            $path[] = $curPath;
        }
    }

    private static function copyFiles(bool $force): void
    {
        foreach (static::getInstallFiles(__DIR__ . '/../Install/') as $file) {
            $destination = preg_replace('{^' . preg_quote(__DIR__ . '/../Install/') . '}', '', $file);
            $destination = preg_replace('{\.txt$}', '', $destination);

            if (false === $force && file_exists($destination)) {
                continue;
            }

            $content = file_get_contents($file);
            file_put_contents($destination, $content);
        }
    }

    private static function getInstallFiles(string $directory, array $allFiles = []): array
    {
        $files = array_diff(scandir($directory), ['.', '..', 'src']);

        foreach ($files as $file) {
            $fullPath = $directory. DIRECTORY_SEPARATOR .$file;

            if( is_dir($fullPath) )
                $allFiles += static::getInstallFiles($fullPath, $allFiles);
            else
                $allFiles[] = str_replace(sprintf('%s%s', DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR, $fullPath);
        }

        return $allFiles;
    }
}
