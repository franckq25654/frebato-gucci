<?php

namespace Frebato\Gucci\Services;

use Illuminate\Http\UploadedFile;
use App\Models\Option as ModelOption;
use Illuminate\Support\Facades\Validator;
use App\Types\File;
use App\Types\Image;
use App\Types\DateTime;
use Illuminate\Support\Carbon;

class Option
{
    public static function store(array $data, ModelOption $option)
    {
        $files = [];

        foreach ($data as $k => $v) {
            if (is_object($v) && UploadedFile::class === get_class($v)) {
                $files[$k] = $v;
                unset($data[$k]);
            }
        }

        $model = ModelOption::buildOption($option->id);
        $rules = null !== request()
            ? Helper::makeRules(request(), $model)
            : [];

        if (count($rules) > 0) {
            $validator = Validator::make(request()->all(), $rules);

            if ($validator->fails()) {
                request()->session()->flash('warning', $validator->messages()->first());

                return null;
            }
        }

        foreach ($files as $property => $uploadedFile) {
            $folder = Image::class === $model->fields[$property]['type']
                ? 'images'
                : 'documents';
            $path = $uploadedFile->storePublicly(sprintf('public/%s', $folder));
            $data[$property] = preg_replace('{^public\/}', 'storage/', $path);
        }

        foreach ($model->dto->fields as $property => $attr) {
            if (in_array($attr['type'], [Image::class, File::class]) && empty(request()?->file($property, ''))) {
                $data[$property] = array_merge($attr, ['value' => request()?->input($property) ?? $data[$property] ?? $model->getAttribute($property)]);
            } elseif (DateTime::class === $attr['type']) {
                $data[$property] = array_merge($attr, ['value' => Carbon::createFromFormat('d/m/Y', request()?->input($property) ?? $data[$property])
                    ->format('Y-m-d 00:00:00')]);
            } elseif ('name' !== $property) {
                $data[$property] = array_merge($attr, ['value' => request()?->input($property) ?? $data[$property] ?? $model->getAttribute($property)]);
            }
        }

        if (null !== request() && !empty(request()->input('name'))) {
            $option->name = request()->input('name');
        }

        $option->value = \json_encode($data, JSON_PRETTY_PRINT);

        return $option;
    }
}
