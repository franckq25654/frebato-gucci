<?php

namespace Frebato\Gucci\Services;

use App\Helpers\Helper;
use App\Traits\HasDTO;
use Exception;

class MakeDtoService
{
    /**
     * @throws Exception
     */
    public static function execute(?string $model = null): void
    {
        static::__init();

        if (null !== $model) {
            $model = sprintf('\\App\\Models\\%s', $model);

            if (class_exists($model)) {
                static::process($model);

                return;
            } else {
                throw new Exception(sprintf('`%s` doesn\'t seem to exist', class_basename($model)));
            }
        }

        foreach (Helper::getModels() as $model) {
            static::process($model);
        }
    }

    private static function __init()
    {
        if (! is_dir(static::getPath())) {
            mkdir(static::getPath());
        }
    }

    private static function process(string $model)
    {
        if (trait_exists(HasDTO::class) && in_array(HasDTO::class, class_uses_recursive($model))) {
            static::createDto($model);
        }
    }

    private static function getPath(): string
    {
        return app_path() . '/DTO';
    }

    private static function createDto(string $model)
    {
        $dtoFilename = static::getPath() . sprintf('/%s.php', class_basename($model));

        if (file_exists($dtoFilename)) {
            return;
        }

        $content = file_get_contents(__DIR__ . '/../Install/src/DTO/ModelDto.php.txt');
        $fields = [];

        foreach (static::getFields($model) as $field) {
            $type = match (true) {
                str_ends_with($field, '_id') => 'TypeModel',
                str_ends_with($field, '_at') => 'DateTime',
                default => 'Text'
            };

            $fields[] = static::segment($field, $type);
        }

        $content = str_replace([
            '#CLASS_NAME#',
            '#DISPLAY_NAME#',
            '#FIELDS#'
        ], [
            class_basename($model),
            ucwords(str_replace('_', ' ', (new $model())->getTable())),
            implode("\n\t\t", $fields)
        ], $content);

        file_put_contents(static::getPath() . sprintf('/%s.php', class_basename($model)), $content);
    }

    private static function getFields(string $model)
    {
        return (new $model())->getFillable();
    }

    private static function segment(string $field, string $type): string
    {
        $segment = <<<SQL
'$field' => [
            'type' => $type::class, // @Todo replace each field by correct type
            'require' => true,
            // 'label' => 'xxxx',
            // 'desc' => '',
        ],
SQL;
        return $segment;
    }
}
