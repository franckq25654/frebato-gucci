<?php

namespace Frebato\Gucci\Services;

use App\Helpers\Helper;
use App\Types\DateTime;
use App\Types\Model as TypeModel;
use App\Types\Password;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File as Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use RuntimeException;

class GucciService
{
    public static function all(Request $request, string $slug, string $routeNamePrefix = '', array $extraButtons = [])
    {
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));
        $model = new $model;

        if (false === $model->dto->dashboardActions('list')) {
            return redirect(route('admin'));
        }

        $dataTableModel = sprintf('App\\DataTables\\%sDataTable', Str::studly(Str::singular($slug)));
        $dataTable = new $dataTableModel($routeNamePrefix);
        $script = $model->dto->adminScriptInline();
        $warning = $request->session()->get('warning');
        $info = $request->session()->get('info');
        $nameDisplay = $model->dto->nameDisplay;
        $routeNew = $model->dto->dashboardActions('store')
            ? route(sprintf('%s.view', $slug), ['id' => 0])
            : null;
        $extraButtons = [];
        $deleteAllButton = $model->dto->dashboardActions('destroy');

        return $dataTable->render(
            'gucci.list',
            compact(
                'warning',
                'info',
                'nameDisplay',
                'routeNew',
                'extraButtons',
                'deleteAllButton',
                'script'
            )
        );
    }

    public static function view(Request $request, string $slug, ?int $id, string $redirectRouteName)
    {
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));
        $model = $model::find($id) ?? new $model;

        if (false === $model->dto->dashboardActions('view')) {
            return redirect(route('admin'));
        }

        $html = $model->dto->asHTML($redirectRouteName);
        $warning = $request->session()->get('warning');
        $info = $request->session()->get('info');

        return view('gucci.view', compact('html', 'warning', 'info'));
    }

    public static function store(Request $request, string $slug, ?int $id): RedirectResponse
    {
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));
        $model = $model::find($id) ?? new $model;

        if (false === $model->dto->dashboardActions('store')) {
            return redirect(route('admin'));
        }

        $rules = Helper::makeRules($request, $model);

        if (count($rules) > 0) {
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $request->session()->flash('warning', $validator->messages()->first());

                return redirect()->back()->withInput();
            }
        }

        if ((int) $model->id > 0) {
            $nbAttrAffected = 0;

            foreach ($model->dto->fields as $property => $attr) {
                if (null === $request->get($property)) {
                    if (in_array($attr['type'], [TypeModel::class, DateTime::class])) {
                        $nbAttrAffected++;
                    }

                    if (TypeModel::class === $attr['type']) {
                        [$relationType, $related, $method] = Helper::getRelationType($model, $property);

                        if (HasMany::class === $relationType) {
                            $related = new $related;
                            $property = Helper::getRelationAttributeByProperty($related, $model::class);
                            $related = $related
                                ->where($property, $model->id)
                                ->first();
                            $related->setAttribute($property, null);
                            $related->save();
                        } elseif (BelongsTo::class === $relationType) {
                            $model->setAttribute($property, null);
                        } elseif (BelongsToMany::class === $relationType) {
                            $model->{$method}()->sync([]);
                        }
                    } elseif (DateTime::class === $attr['type']) {
                        $model->setAttribute($property, null);
                    }
                }
            }

            if ($nbAttrAffected > 0) {
                $model->saveQuietly();
            }
        }

        foreach ($request->all() as $property => $value) {
            if (isset($model->dto->fields[$property])) {
                $attr = $model->dto->fields[$property];

                if (DateTime::class === $attr['type']) {
                    if (null === $value) {
                        $model->setAttribute($property, null);
                    } else {
                        try {
                            $model->setAttribute($property, Carbon::createFromFormat('d/m/Y', $value));
                        } catch (InvalidFormatException $e) {
                            $model->setAttribute($property, new Carbon($value));
                        }
                    }
                } elseif (Password::class === $attr['type']) {
                    $model->setAttribute($property, Hash::make($value));
                } elseif (TypeModel::class !== $attr['type']) {
                    $model->setAttribute($property, $value);
                }
            }
        }

        try {
            $model->save();
        } catch (\Exception $e) {
            $request->session()->flash('warning', $e->getMessage());

            return redirect()->back()->withInput();
        }

        foreach ($request->all() as $property => $value) {
            if (isset($model->dto->fields[$property])) {
                $attr = $model->dto->fields[$property];

                if (TypeModel::class === $attr['type']) {
                    [$relationType, $related, $method] = Helper::getRelationType($model, $property);

                    if (HasMany::class === $relationType) {
                        $related = new $related;
                        $property = Helper::getRelationAttributeByProperty($related, $model::class);
                        $related = $related
                            ->where($property, $model->id)
                            ->first();
                        $related->setAttribute($property, $value);
                        $related->save();
                    } elseif (BelongsTo::class === $relationType) {
                        $model->setAttribute($property, $value);
                        $model->saveQuietly();
                    } elseif (BelongsToMany::class === $relationType) {
                        $model->{$method}()->sync($value);
                    }
                }
            }
        }

        $action = (int) $id <= 0
            ? 'crée'
            : 'modifiée';

        $request->session()->flash(
            'info',
            sprintf('La ressource %s a bien été %s.', Str::singular($model->getTable()), $action)
        );

        return redirect($request->input('redirect-to-referer'));
    }

    public static function destroy(Request $request, string $slug, int $id): JsonResponse
    {
        $response = new JsonResponse();
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));
        $item = (new $model)->find($id);

        if (false === $item->dto->dashboardActions('destroy')) {
            return $response->setData([
                'status' => 'error',
                'previous_url' => url()->previous(),
                'message' => $request->session()->get('warning'),
            ]);
        }

        $item->forceDelete();

        $message = sprintf('La ressource %s a bien été placée dans la corbeille.', Str::singular($slug));

        return $response->setData([
            'status' => 'success',
            'previous_url' => url()->previous(),
            'message' => $message,
        ]);
    }

    public static function forceDestroy(Request $request, string $slug, int $id): JsonResponse
    {
        $response = new JsonResponse();
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));
        $item = (new $model)->find($id);

        if (false === $item->dto->dashboardActions('force-destroy')) {
            return $response->setData([
                'status' => 'error',
                'previous_url' => url()->previous(),
                'message' => $request->session()->get('warning'),
            ]);
        }

        (new $model)::withTrashed()->whereId($id)->firstOrFail()->forceDelete();

        $message = sprintf(
            'La ressource %s a bien été supprimée définitivement dans la base de données.',
            Str::singular($slug)
        );

        return $response->setData([
            'status' => 'success',
            'previous_url' => url()->previous(),
            'message' => $message,
        ]);
    }

    public static function restore(Request $request, string $slug, int $id): JsonResponse
    {
        $response = new JsonResponse();
        $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));

        if (false === (new $model())->dto->dashboardActions('restore')) {
            return $response->setData([
                'status' => 'error',
                'previous_url' => url()->previous(),
                'message' => $request->session()->get('warning'),
            ]);
        }

        (new $model)::withTrashed()->whereId($id)->firstOrFail()->restore();
        $message = sprintf('La ressource %s a bien été restaurée.', Str::singular($slug));

        return $response->setData([
            'status' => 'success',
            'previous_url' => url()->previous(),
            'message' => $message,
        ]);
    }

    public static function deleteAttachment(Request $request, string $slug, int $id, string $name): JsonResponse
    {
        $response = new JsonResponse();

        if ($request->ajax()) {
            $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));

            if ($model = $model::find($id)) {
                $isRemoved = Storage::delete(preg_replace(
                    '{^storage\/}',
                    storage_path('app/public/'), $model->{$name}
                ));

                if (!$isRemoved) {
                    throw new \RuntimeException('fichier introuvable', Response::HTTP_BAD_REQUEST);
                }

                $model->{$name} = null;
                $model->save();

                $message = sprintf(
                    'La ressource %s a bien été supprimée définitivement dans la base de données.',
                    Str::singular($slug)
                );

                return $response->setData([
                    'status' => 'success',
                    'previous_url' => url()->previous(),
                    'message' => $message,
                ]);
            }
        }

        throw new RuntimeException(__('globals.ajax.error'), Response::HTTP_BAD_REQUEST);
    }

    public static function addResource(Request $request, string $slug): JsonResponse
    {
        $response = new JsonResponse();

        if ($request->ajax()) {
            $model = sprintf('App\\Models\\%s', Str::studly(Str::singular($slug)));

            foreach ($request->all() as $name => $value) {
                $item = new $model;
                $item->{$name} = $value;
            }

            $item->save();
        }

        $message = sprintf(
            'La ressource %s a bien été crée à la volée.',
            Str::singular($slug)
        );

        return $response->setData([
            'status' => 'success',
            'previous_url' => url()->previous(),
            'message' => $message,
        ]);
    }
}
