<?php

use App\Helpers\Helper;
use Frebato\Gucci\Http\GucciController;
use Frebato\Gucci\Http\OptionController;
use Illuminate\Support\Facades\Route;

if (class_exists(Helper::class)) {
    foreach (Helper::getModels() as $model) {
        $slug = (new $model)->getTable();

        Route
            ::get(
                sprintf('/admin/%s', $slug),
                [GucciController::class, 'all']
            )
            ->name(sprintf('%s.all', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::get(
                sprintf('/admin/%s/{id}', $slug),
                [GucciController::class, 'view']
            )
            ->name(sprintf('%s.view', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::match(
                ['post', 'put'],
                sprintf('/admin/%s/save/{id}', $slug),
                [GucciController::class, 'store']
            )
            ->name(sprintf('%s.store', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::delete(
                sprintf('/admin/%s/destroy/{id}', $slug),
                [GucciController::class, 'destroy']
            )
            ->name(sprintf('%s.destroy', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::delete(
                sprintf('/admin/%s/force-destroy/{id}', $slug),
                [GucciController::class, 'forceDestroy']
            )
            ->name(sprintf('%s.force-destroy', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::put(
                sprintf('/admin/%s/restore/{id}', $slug),
                [GucciController::class, 'restore']
            )
            ->name(sprintf('%s.restore', $slug))
            ->middleware(['web', 'auth'])
        ;

        /**
         * Ajax Request method
         */
        Route
            ::post(
                sprintf('/admin/add-resource/%s/{id}/{name}', $slug),
                [GucciController::class, 'addResource']
            )
            ->name(sprintf('%s.ajax.item-add', $slug))
            ->middleware(['web', 'auth'])
        ;

        Route
            ::delete(
                sprintf('/admin/delete-attachment/%s/{id}/{name}', $slug),
                [GucciController::class, 'deleteAttachment']
            )
            ->name(sprintf('%s.delete-attachment', $slug))
            ->middleware(['web', 'auth'])
        ;
    }
}

Route
    ::get(
        '/admin/options',
        [OptionController::class, 'all']
    )
    ->name('options.all')
    ->middleware(['web', 'auth'])
;

Route
    ::get(
        '/admin/options/{id}',
        [OptionController::class, 'view']
    )
    ->name('options.view')
    ->middleware(['web', 'auth'])
;

Route
    ::match(
        ['post', 'put'],
        '/admin/options/save/{id}',
        [OptionController::class, 'store']
    )
    ->name('options.store')
    ->middleware(['web', 'auth'])
;

Route
    ::get(
        '/leave/impersonate',
        [GucciController::class, 'leave']
    )
    ->name('frebato.impersonate.leave')
    ->middleware(['web'])
;
