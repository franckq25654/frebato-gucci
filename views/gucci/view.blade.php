@extends('gucci.base')
@section('title') {{ __('globals.title.homepage') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            {!! $html !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            if ($('#end_date').length) {
                $('#start_date').on('dp.change', function (e) {
                    var start_date = $(this).data('date');
                    var end_date = moment(start_date, "DD/MM/YYYY").add(7, 'days').toDate();
                    $('#end_date').data("DateTimePicker").date(new Date(end_date));
                });
            }
        });
    </script>
@endsection
