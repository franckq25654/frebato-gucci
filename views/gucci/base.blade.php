<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="SVP Espace-client V2">
    <meta name="author" content="SVP DSI">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="access_token" content="">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000000">
    <title>{{ config('app.name') }}</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome/css/brands.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome/css/solid.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/iziToast.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    @vite('resources/css/app.css')
    @yield('css')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light position-relative py-4">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            @guest
                <li class="nav-item d-sm-inline-block">
                    <a href="{{ route('login') }}"><i class="fas fa-user-circle"></i></a>
                </li>
            @endguest
            @auth
                <li class="nav-item d-sm-inline-block">
                    <form action="{{ route('logout') }}" method="POST" hidden>
                        @csrf
                    </form>
                    <a class="nav-link"
                       href="{{ route('logout') }}"
                       title="@lang('Logout')"
                       onclick="event.preventDefault(); this.previousElementSibling.submit();">
                        <i class="fas fa-power-off"></i>
                    </a>
                </li>
            @endauth
        </ul>
        @auth
            @if(null !== $user?->logo && file_exists(\App\Helpers\Helper::pathByUrl($user?->logo)))
                <div id="header-logo" style="background-image: url({{ config('app.url') . $user?->logo }})"></div>
            @else
                <p class="img-fluid position-absolute top-0 end-0 mt-4 mr-4 d-none d-sm-block">Accès {{ $user->name }}</p>
            @endif
        @endauth
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <img id="logo" src="{{ asset('images/logo.png') }}" class="img-fluid mt-2 mb-5" alt="" />
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    @foreach($menu as $name => $elements)
                        @if(is_array($elements['min-level']) && in_array(auth()->user()?->role, $elements['min-level']) || $elements['min-level'] <= auth()->user()?->role)
                            @isset($elements['children'])
                                <li class="nav-item has-treeview {{ \App\Helpers\Helper::menuOpen($elements['children']) }}">
                                    <a href="{{ $elements['route'] }}" class="nav-link {{ \App\Helpers\Helper::currentChildActive($elements['children']) }}">
                                        <i class="nav-icon fas fa-{{ $elements['icon'] }}"></i>
                                        <p>
                                            @lang($name)
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @foreach($elements['children'] as $_name => $child)
                                            @if(is_array($child['min-level']) && in_array(auth()->user()?->role, $child['min-level']) || $child['min-level'] <= auth()->user()?->role)
                                                <x-gucci.menu-item
                                                    :route="$child['route']"
                                                    :sub=true>
                                                    @lang($_name)
                                                </x-gucci.menu-item>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <x-gucci.menu-item
                                    :route="$elements['route']"
                                    :icon="$elements['icon']">
                                    @lang($name)
                                </x-gucci.menu-item>
                            @endisset
                        @endif
                    @endforeach
                </ul>
            </nav>
        </div>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark">{{ $nameDisplay ?? $title }}</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 {{ config('app.name', 'Laravel') }}.</strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery-3.6.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery.mask.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery.rebind.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/moment/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/bootstrap/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/iziToast.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/adminlte.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@vite('resources/js/app.js')
@yield('js')
@if(!empty($warning))
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            iziToast.show({
                message: '{{ $warning }}',
                color: 'red',
                timeout: 120000
            });
        });
    </script>
@endif

@if(!empty($info))   <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            iziToast.show({
                message: '{{ $info }}',
                color: 'green',
                timeout: 120000
            });
        });
    </script>
@endif
@if(\App\Helpers\Helper::isImpersonating())
    <div id="impersonnate">
        En tant que {{ $user->firstname }} {{ $user->name }}<br />
        <a href="{{ route('frebato.impersonate.leave') }}"><b>Quittez <i class="fas fa-sign-out-alt"></i></b></a>
    </div>
@endif
</body>
</html>
