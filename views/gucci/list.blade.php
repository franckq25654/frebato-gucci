@extends('gucci.base')
@section('title') {{ __('globals.title.homepage') }}
@endsection
@section('css')
    <style>
        a > * { pointer-events: none; }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row mb-5">
            <div id="btns-action-group" class="btn-group" role="group">
                @if (null !== $routeNew)
                    <a
                        class="btn btn-success mx-1"
                        href="{{ $routeNew }}"
                        role="button">
                        <i class="fas fa-plus"></i>
                        Nouveau
                    </a>
                @endif
                @foreach ($extraButtons as $extraButton)
                    <a
                        class="btn {{ !empty($extraButton['class']) ? $extraButton['class'] : 'btn-primary' }} mx-1"
                        href="{{ $extraButton['url'] }}"
                        role="button">
                        <i class="{{ $extraButton['icon'] }}"></i>
                        {{ $extraButton['label'] }}
                    </a>
                @endforeach
                @if(true === $deleteAllButton)
                    <button
                        id="delete-selected"
                        type="button"
                        class="btn btn-success mx-1"
                        disabled>
                        <i class="fas fa-trash"></i>
                        Supprimer
                    </button>
                @endif
            </div>
        </div>
        <div class="row">
            {{ $dataTable->table(['class' => 'table table-bordered table-hover table-sm'], true) }}
        </div>
    </div>
@endsection
@section('js')
    @if(null !== $script)
    <script type="text/javascript">
        {!! $script !!}
    </script>
    @endif
    <script type="text/javascript">
        (($, DataTable) => {
            $.extend(true, DataTable.defaults, {
                language: {
                    "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                    "sInfo":           "Affichage des éléments _START_ à _END_ sur _TOTAL_ éléments",
                    "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                    "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "Afficher _MENU_ éléments",
                    "sLoadingRecords": "Chargement...",
                    "sProcessing":     "Traitement...",
                    "sSearch":         "Rechercher :",
                    "sZeroRecords":    "Aucun élément correspondant trouvé",
                    "oPaginate": {
                        "sFirst":    "Premier",
                        "sLast":     "Dernier",
                        "sNext":     "Suivant",
                        "sPrevious": "Précédent"
                    },
                    "oAria": {
                        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                    },
                    "select": {
                        "rows": {
                            "_": "%d lignes sélectionnées",
                            "0": "Aucune ligne sélectionnée",
                            "1": "1 ligne sélectionnée"
                        }
                    }
                },
                paging: false,
                "drawCallback": function() {
                    $(document).trigger('dt.draw');
                }
            });
        })(jQuery, jQuery.fn.dataTable);
    </script>
    {{ $dataTable->scripts() }}
@endsection
