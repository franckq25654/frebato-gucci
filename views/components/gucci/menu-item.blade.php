@props(['route', 'sub', 'icon'])
<li class="nav-item">
    <a href="{{ $route }}" class="nav-link {{ \App\Helpers\Helper::currentRouteActive($route) }}">
        <i class="
        @isset($sub)
            far fa-circle
        @endisset
            nav-icon
        @isset($icon)
            fas fa-{{ $icon }}
        @endisset
            "></i>
        <p>{{ $slot }}</p>
    </a>
</li>
