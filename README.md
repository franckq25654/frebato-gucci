### Préambule
```
nano ~/.bashrc

alias sail='bash vendor/bin/sail'

source ~/.bashrc
```
## Gucci ##
### Installation ###

Add Gucci to your composer.json file :
```
sudo chown -R $(whoami):$(whoami) . && chmod -R 755 .
sail shell
composer require frebato/gucci:dev-master -W
```

Update Composer :
```
composer update
composer dump-autoload
```

The next required step is to add the service provider to <ins>config/app.php</ins> :
```
Frebato\Gucci\ServiceProvider::class,
Lab404\Impersonate\ImpersonateServiceProvider::class,
```

### Publish ###

The last required step is to publish views and assets in your application with :
```
php artisan ui vue --auth
php artisan vendor:publish --tag=frebato-gucci
```
```
php artisan gucci:install
```
after that
```
composer dump-autoload
```
<ul>
    <li>add <i>use HasDTO;</i> trait in <b>each</b> Models</li>
    <li>add <i>use HasRole;</i> trait in <b>User</b> Model</li>
</ul>

```
php artisan gucci:make-dto
```
After that, customize DTO file generate in <ins>/App/DTO/</ins>, then :
```
php artisan gucci:make-datatables
php artisan gucci:generate-migrations
php artisan migrate
php artisan db:seed
npm update
npm run build
```
Then, go for a walk on the side of <ins>config/menu.php</ins> to customize the admin panel

### Finalize configuration ###
Add
 this at bottom of routes/web.php file :

```
require __DIR__.'/gucci.php';
```

Congratulations, you have successfully installed Gucci !
